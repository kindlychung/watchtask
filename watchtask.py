#!/usr/bin/env python3


import sys
import os

from PySide import QtGui

from ui.mainwindow import Ui_MainWindow
from utils.taskmodel import TaskModel


class Prog(QtGui.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.actionAdd.triggered.connect(self.addtask)
        self.actionStart.triggered.connect(self.starttask)
        self.actionStop.triggered.connect(self.stoptask)
        self.homedir = os.getenv("HOME")
        self.model = TaskModel()
        self.tableView.setModel(self.model)
        self.tableView.horizontalHeader().setResizeMode(QtGui.QHeaderView.Stretch)

    def addtask(self):
        self.model.insertRow(0)
        return True

    def starttask(self):
        indexes = self.tableView.selectionModel().selectedIndexes()
        if indexes:
            rows = [i.row() for i in indexes]
            rows = set(rows)
            for row in rows:
                self.model.starttask(row)
            self.model.dataChanged.emit(indexes[0], indexes[-1])
            return True
        else:
            return False

    def stoptask(self):
        indexes = self.tableView.selectionModel().selectedIndexes()
        if indexes:
            rows = [i.row() for i in indexes]
            rows = set(rows)
            for row in rows:
                self.model.stoptask(row)
                self.model.removeRow(row)
            return True
        else:
            return False





app = QtGui.QApplication(sys.argv)
prog = Prog()
prog.show()
sys.exit(app.exec_())
