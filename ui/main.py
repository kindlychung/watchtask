# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created: Fri Sep 19 22:41:27 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Prog(object):
    def setupUi(self, Prog):
        Prog.setObjectName("Prog")
        Prog.resize(560, 349)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Prog.sizePolicy().hasHeightForWidth())
        Prog.setSizePolicy(sizePolicy)
        Prog.setMinimumSize(QtCore.QSize(560, 349))
        Prog.setWindowOpacity(1.0)
        self.layoutWidget = QtGui.QWidget(Prog)
        self.layoutWidget.setGeometry(QtCore.QRect(20, 21, 520, 311))
        self.layoutWidget.setObjectName("layoutWidget")
        self.verticalLayout = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.tableView = QtGui.QTableView(self.layoutWidget)
        self.tableView.setObjectName("tableView")
        self.verticalLayout.addWidget(self.tableView)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.addbtn = QtGui.QPushButton(self.layoutWidget)
        self.addbtn.setObjectName("addbtn")
        self.horizontalLayout.addWidget(self.addbtn)
        self.startbtn = QtGui.QPushButton(self.layoutWidget)
        self.startbtn.setObjectName("startbtn")
        self.horizontalLayout.addWidget(self.startbtn)
        self.stopbtn = QtGui.QPushButton(self.layoutWidget)
        self.stopbtn.setObjectName("stopbtn")
        self.horizontalLayout.addWidget(self.stopbtn)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(Prog)
        QtCore.QMetaObject.connectSlotsByName(Prog)

    def retranslateUi(self, Prog):
        Prog.setWindowTitle(QtGui.QApplication.translate("Prog", "WatchTask", None, QtGui.QApplication.UnicodeUTF8))
        Prog.setAccessibleName(QtGui.QApplication.translate("Prog", "Watch folder, fire command on change", None, QtGui.QApplication.UnicodeUTF8))
        self.addbtn.setText(QtGui.QApplication.translate("Prog", "Add", None, QtGui.QApplication.UnicodeUTF8))
        self.startbtn.setText(QtGui.QApplication.translate("Prog", "Start", None, QtGui.QApplication.UnicodeUTF8))
        self.stopbtn.setText(QtGui.QApplication.translate("Prog", "Stop", None, QtGui.QApplication.UnicodeUTF8))

