# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'watchdialog.ui'
#
# Created: Fri Sep 19 22:40:20 2014
#      by: pyside-uic 0.2.15 running on PySide 1.2.2
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_WatchDialog(object):
    def setupUi(self, WatchDialog):
        WatchDialog.setObjectName("WatchDialog")
        WatchDialog.resize(562, 188)
        self.layoutWidget = QtGui.QWidget(WatchDialog)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 10, 541, 161))
        self.layoutWidget.setObjectName("layoutWidget")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.layoutWidget)
        self.verticalLayout_2.setSpacing(-1)
        self.verticalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.getDirEdit = QtGui.QLineEdit(self.layoutWidget)
        self.getDirEdit.setObjectName("getDirEdit")
        self.horizontalLayout.addWidget(self.getDirEdit)
        self.getDirBtn = QtGui.QPushButton(self.layoutWidget)
        self.getDirBtn.setObjectName("getDirBtn")
        self.horizontalLayout.addWidget(self.getDirBtn)
        self.recursivecheck = QtGui.QCheckBox(self.layoutWidget)
        self.recursivecheck.setObjectName("recursivecheck")
        self.horizontalLayout.addWidget(self.recursivecheck)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.cmdedit = QtGui.QLineEdit(self.layoutWidget)
        self.cmdedit.setObjectName("cmdedit")
        self.verticalLayout.addWidget(self.cmdedit)
        self.verticalLayout_2.addLayout(self.verticalLayout)
        self.patternsEdit = QtGui.QLineEdit(self.layoutWidget)
        self.patternsEdit.setText("")
        self.patternsEdit.setObjectName("patternsEdit")
        self.verticalLayout_2.addWidget(self.patternsEdit)
        self.buttonBox = QtGui.QDialogButtonBox(self.layoutWidget)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout_2.addWidget(self.buttonBox)

        self.retranslateUi(WatchDialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), WatchDialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), WatchDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(WatchDialog)

    def retranslateUi(self, WatchDialog):
        WatchDialog.setWindowTitle(QtGui.QApplication.translate("WatchDialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.getDirEdit.setPlaceholderText(QtGui.QApplication.translate("WatchDialog", "Folder that you want to watch", None, QtGui.QApplication.UnicodeUTF8))
        self.getDirBtn.setText(QtGui.QApplication.translate("WatchDialog", "Browse", None, QtGui.QApplication.UnicodeUTF8))
        self.recursivecheck.setText(QtGui.QApplication.translate("WatchDialog", "Recursive", None, QtGui.QApplication.UnicodeUTF8))
        self.cmdedit.setPlaceholderText(QtGui.QApplication.translate("WatchDialog", "Command you want to execute on change", None, QtGui.QApplication.UnicodeUTF8))
        self.patternsEdit.setPlaceholderText(QtGui.QApplication.translate("WatchDialog", "Separate by comma, for example *.txt,*.html", None, QtGui.QApplication.UnicodeUTF8))

