import os
from watchdog.events import PatternMatchingEventHandler

def makewatchHandler(pat=[]):
    class WatchHandler(PatternMatchingEventHandler):
        if pat:
            if not isinstance(pat, list):
                raise ValueError
            patterns = pat
        def __init__(self, cmd=""):
            super().__init__()
            if not isinstance(cmd, str):
                raise ValueError("cmd must be a string (to be passed to os.system)")
            self.cmd = cmd
        def process(self, event):
            os.system(self.cmd)
        def on_created(self, event):
            self.on_modified(event)
        def on_modified(self, event):
            self.process(event)

    return WatchHandler

# from watchdog.observers import Observer
# obs = Observer()
# WatchHandler = makewatchHandler(["*.html"])
# obs.schedule(WatchHandler("afplay ~/bell.wav"), path="/tmp/testwatch", recursive=True)
# obs.start()
# obs.stop()

