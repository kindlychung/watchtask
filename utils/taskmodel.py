from PySide import QtGui, QtCore
from .getwatchEntry import getwatchEntry, WatchDiag
from watchdog.observers import Observer

class TaskModel(QtCore.QAbstractTableModel):
    def __init__(self, tasks=None):
        super().__init__()
        self.__tasks = [] if tasks is None else tasks
        self.__headers = ["Folder", "Command", "Patterns", "Active", "Recursive"]

    def rowCount(self, *args, **kwargs):
        return len(self.__tasks)

    def columnCount(self, *args, **kwargs):
        if self.__tasks:
            coln = len(self.__tasks[0])
        else:
            coln = len(self.__headers)
        return coln

    def headerData(self, section, orientation, role):
        if role == QtCore.Qt.DisplayRole:
            if orientation == QtCore.Qt.Horizontal:
                return self.__headers[section]
            else:
                # set row names: color 0, color 1, ...
                return "%s" % str(section+1)

    def data(self, index, role):
        row = index.row()
        col = index.column()
        if col == 3:
            value = str(self.__tasks[row][col].isAlive())
        else:
            value = self.__tasks[row][col]

        # set an icon for Active data entry
        if role == QtCore.Qt.DecorationRole:
            if col == 3:
                pixmap = QtGui.QPixmap(10, 10)
                if value == "True":
                    pixmap.fill(QtGui.QColor(11, 249, 25))
                else:
                    pixmap.fill(QtGui.QColor(249, 11, 25))
                icon = QtGui.QIcon(pixmap)
                return icon

        # text content
        if role == QtCore.Qt.DisplayRole:
            return value


    def insertRows(self, position, rows, parent=QtCore.QModelIndex()):
        self.beginInsertRows(parent, position, position + rows - 1)

        row = getwatchEntry()
        if not row:
            return False

        for i in range(rows):
            self.__tasks.insert(position, row)

        self.endInsertRows()
        return True

    def removeRows(self, position, rows, parent=QtCore.QModelIndex()):
        self.beginRemoveRows(parent, position, position + rows -1)
        for i in range(rows):
            del self.__tasks[position]
        self.endRemoveRows()
        return True


    def starttask(self, n):
        if not self.__tasks[n][3].isAlive():
            self.__tasks[n][3].start()

    def stoptask(self, n):
        if self.__tasks[n][3].isAlive():
            self.__tasks[n][3].stop()

    def startall(self):
        for row in self.__tasks:
            if not row[3].isAlive():
                row[3].start()

    def stopall(self):
        for row in self.__tasks:
            if row[3].isAlive():
                row[3].stop()




