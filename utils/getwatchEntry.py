import os
import re

from PySide import QtGui
from watchdog.observers import Observer

from ui.watchdialog import Ui_WatchDialog
from .watchhandler import makewatchHandler


class WatchDiag(QtGui.QDialog, Ui_WatchDialog):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.getDirBtn.clicked.connect(self.open)
    def open(self):
        dir = "."
        dirselected = QtGui.QFileDialog.getExistingDirectory(caption = "Open folder", dir=dir)
        self.getDirEdit.setText(dirselected)

def getwatchEntry():
    homedir = os.getenv("HOME")
    mywatch = WatchDiag()

    if mywatch.exec_():
        watchdir = mywatch.getDirEdit.text().strip()
        watchdir = watchdir if watchdir else homedir

        watchPatterns = mywatch.patternsEdit.text()
        watchPatterns = watchPatterns.split(",")
        watchPatterns = [x.strip() for x in watchPatterns]

        matchlist = set([bool(re.match(r"^\*\.\w+$", i)) for i in watchPatterns])
        if False in matchlist:
            QtGui.QMessageBox.warning(self, "Something wrong", "Invalid pattern(s), I'll do nothing.")
            return False

        cmd = mywatch.cmdedit.text().strip()
        if not cmd:
            QtGui.QMessageBox.warning(self, "Something wrong", "No command provided!")
            return False


        obs = Observer()
        WatchHandler = makewatchHandler(watchPatterns)
        isRecursive = mywatch.recursivecheck.isChecked()
        obs.schedule(WatchHandler(cmd), path=watchdir, recursive=isRecursive)
        # obs.start()

        return [watchdir, cmd, " ".join(watchPatterns), obs, str(isRecursive)]

